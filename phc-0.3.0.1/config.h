/* config.h.  Generated from config.h.in by configure.  */
/*
 * phc -- the open source PHP compiler
 * See doc/license/README.license for licensing information
 *
 * configure settings
 */

/*
 * Package configuration 
 */

#define PACKAGE "phc"
#define VERSION "0.3.0"

/*
 * Garbage collector
 */

// --enable-gc specified? 
/* #undef DISABLE_GC */

/*
 * Xerces-C++
 */

// Xerces-C++ available?
/* #undef HAVE_XERCES */

/*
 * PHP
 */

// Embed SAPI available
/* #undef HAVE_EMBED */

// PHP installation path
#define PHP_INSTALL_PATH "/usr/local"
