<?php

function parse_win_lang()
{
    $ret = array();
    $cmd = 'systeminfo';
    exec($cmd,$ret);
    var_dump($ret);
}

function parse_linux_lang()
{
    $ret = array();
    $cmd = 'locale | grep LANG=';
    exec($cmd,$ret);
    $str = reset($ret);
    $tmp = explode('=', $str);
    $lang = substr($tmp[1], 0,2);
    return  $lang;
}

// DEFINE

$os = (strtoupper(substr(php_uname('s'), 0, 3)) === 'WIN' ? 'windows' : 'linux');
define('OS', $os);

$lang = ( OS == 'windows' ? parse_win_lang() : parse_linux_lang() );
define('LANG', $lang);


define('DS', DIRECTORY_SEPARATOR);

define('ROOT_DIR', __DIR__ . DS);
//define('WP_DIR_NAME', ROOT_DIR . 'mywordpress');
define('CONFIG_FILE', ROOT_DIR . 'conf.ini');

// modification de $PATH l'autoload
set_include_path(implode(PATH_SEPARATOR, array(
    __DIR__,
    __DIR__ . DIRECTORY_SEPARATOR .'libs'. DIRECTORY_SEPARATOR .'ConsoleKit' . DIRECTORY_SEPARATOR .'src',
    __DIR__ . DIRECTORY_SEPARATOR .'commands',
    get_include_path()
    )));

spl_autoload_register(function($className) {
    if (substr($className, 0, 10) === 'ConsoleKit') {
     $filename = str_replace('\\', DIRECTORY_SEPARATOR, trim($className, '\\')) . '.php';
     require_once $filename;
 }
 elseif(substr($className, 0, 2) === 'Wp')
   require_once 'commands' . DIRECTORY_SEPARATOR . $className . '.php';
});

