<?php 

use ConsoleKit\Console,
ConsoleKit\Command,
ConsoleKit\Colors,
ConsoleKit\FileSystem,
ConsoleKit\Utils,
ConsoleKit\Widgets\Dialog,
ConsoleKit\Widgets\ProgressBar;

class WpInstall extends Command
{

	private $dialog;

	private $defaultInstallDir;


	private function extract_wp($filename)
	{
		if(OS == 'windows')
		{

		}
		else
		{
			$gz = new PharData($filename);
			$gz->decompress();
			$newfilename = substr($filename, 0, strpos($filename, '.gz'));

			//$extract_path = substr($filename, 0, strpos($filename, '.tar'));
			$tar = new PharData($newfilename);
			echo 'doc root : ' . get_doc_root() . "\n";
			$tar->extractTo(get_doc_root());

			unlink($filename);
			unlink($newfilename);
		}

	}

	private function dl_wordpress()
	{
		$extension = (OS == 'windows' ? 'zip' : 'tar.gz');

		$url = (LANG == 'fr' ? 'http://fr.wordpress.org/latest-fr_FR.' . $extension : 'http://wordpress.org/latest.'. $extension);

		echo $dl_path = $this->defaultInstallDir . DS . 'toto.' . $extension;

		// si le telechargement est reussi
		if(!file_put_contents($dl_path, fopen($url, 'r')))
			echo "download error.\n";
		$this->extract_wp($dl_path);

	}

	private function config_db()
	{
		$config = parse_ini_file(CONFIG_FILE,true);
		
		// renommer wp-config-sample.php en wp-onfig.php

		$base = $this->defaultInstallDir  . DIRECTORY_SEPARATOR . 'wordpress' . DIRECTORY_SEPARATOR;

		$dbfilename = $base .'wp-config-sample.php';
		$newdbfilename = $base .'wp-config.php';
		if(file_exists($dbfilename))
			rename($dbfilename, $newdbfilename);

			// ici edition fichier wp-config.php
		$strfile = file($newdbfilename);
		$newstrfile = str_replace(
			array(
				( LANG == 'fr' ? 'votre_nom_de_bdd'			: 'database_name_here'),
				( LANG == 'fr' ? 'votre_utilisateur_de_bdd' : 'username_here' ),
				( LANG == 'fr' ? 'votre_mdp_de_bdd'			: 'password_here'),
				'localhost',
				'utf8'
				),
			array(
				$config['DATABASE']['DB_NAME'],
				$config['DATABASE']['DB_USERNAME'],
				$config['DATABASE']['DB_PASSWORD'],
				$config['DATABASE']['DB_HOST'],
				$config['DATABASE']['DB_CHARSET']

				), $strfile);
		$newstrfile = implode('', $newstrfile);

		if(file_put_contents($newdbfilename, $newstrfile))
			echo "fichier wp-config.php edite avec succes.\n";
	}

	private function config_wp()
	{

			$conf = parse_ini_file (CONFIG_FILE, true);
			$doc_root = $conf['SYSTEM']['DOCROOT'];
			$install_uri = 'http://localhost/wordpress/wp-admin/install.php?step=2';

			if(!$ch = curl_init($install_uri))
				throw new Exception("curl init failed");

			$options = array(
				CURLOPT_POSTFIELDS 		=> array(
					'weblog_title' 		=> 'my title',
					'user_name' 		=> $conf['ADMIN']['USERNAME'],
					'admin_password' 	=> $conf['ADMIN']['PWD'],
					'admin_password2' 	=> $conf['ADMIN']['PWD'],
					'admin_email' 		=> 'toto@mail.fr',
					'blog_public'		=> 0,
					'Submit' 			=> urlencode('Install WordPress')
					),
				CURLOPT_RETURNTRANSFER => true
				);

			curl_setopt_array($ch, $options);

			$data = curl_exec($ch);
			
			curl_close($ch);

		}

	}
	private function init()
	{
		$this->dialog = new Dialog($this->console);

		$tmp = parse_ini_file(CONFIG_FILE,true);
		
		$doc_root = $tmp['SYSTEM']['DOCROOT'];
		
		$this->defaultInstallDir = $doc_root;

	}

	public function execute(array $args, array $options = array())
	{
		$this->init();
		if(!file_exists($this->defaultInstallDir))
		{
			$this->writeln($this->defaultInstallDir . " doesn't exists on disk,");
			if($this->dialog->confirm('do you want to create directory ?'))
				FileSystem::mkdir($this->defaultInstallDir);
		}

		echo "debut du telechargement \n";
		
		$this->dl_wordpress();

		$this->config_db();

		$this->config_wp();

	}
}