<?php

use ConsoleKit\Console,
ConsoleKit\Command,
ConsoleKit\Colors,
ConsoleKit\FileSystem,
ConsoleKit\Utils,
ConsoleKit\Widgets\Dialog,
ConsoleKit\Widgets\ProgressBar;

class APP extends ConsoleKit\Console{


	public $console;

	public function __construct()
	{
		$this->console = new ConsoleKit\Console();
	}

private function getConfigFile()
	{
		return file('example.conf.ini');
	}

private function registerCommands()
	{
		$coms = array();
		$comands = scandir('commands');
		unset($comands[array_search('.', $comands)]);
		unset($comands[array_search('..', $comands)]);
		reset($comands);

		foreach ($comands as $value) {
			$name = substr($value,0, strpos($value,'.php'));
			$this->console->addCommand($name);
		}
	}

	private function init()
	{
		if(!file_exists(ROOT_DIR . 'conf.ini'))
			FileSystem::touch(ROOT_DIR . 'conf.ini', $this->getConfigFile());

		$this->registerCommands();
	}

	


	public function run()
	{
		$this->init();
		$dialog = new Dialog($this->console);
		$docRoot = $dialog->ask('path to your document root : ',false);
		if($docRoot === false)
			die("Error : you must enter a path to your document_root \n");
		$configfile = ROOT_DIR . 'conf.ini';


		$strfile = file($configfile);
		$newstrfile = str_replace(
			array(
				"DOCROOT = ''",
				),
			array(
				"DOCROOT = " . $docRoot
				), $strfile);
		$newstrfile = implode('', $newstrfile);

		if(!file_put_contents($configfile, $newstrfile))
			throw new Exception("error editing conf.ini DOCROOT value", 1);
		
		$this->console->run();
	}

}


?>